package consiste;

public class Pessoa {
	
	private String primeiroNome;
	private String sobreNome;
	private double peso;
	private double altura;
	private double imc;
	
	
	public Pessoa(String primeiroNome, String sobreNome, double peso, double altura, double imc) {
		this.primeiroNome = primeiroNome;
		this.sobreNome = sobreNome;
		this.peso = peso;
		this.altura = altura;
		this.imc = imc;
				
	}

	 String getPrimeiroNome() {
		return primeiroNome;
	}

	

	 String getSobreNome() {
		return sobreNome;
	}

	 void setSobreNome(String sobreNome) {
		this.sobreNome = sobreNome;
	}

	 double getPeso() {
		return peso;
	}

	 void setPeso(double peso) {
		this.peso = peso;
	}

	 double getAltura() {
		return altura;
	}

	 void setAltura(double altura) {
		this.altura = altura;
	}

	@Override
	public String toString() {
		return "Pessoa [primeiroNome=" + primeiroNome + ", sobreNome=" + sobreNome + ", peso=" + peso + ", altura="
				+ altura + "]";
	}

	 double getImc() {
		return imc;
	}

	 void setImc(double imc) {
		this.imc = imc;
	}
	
	
	
	
	

	
	

	
	
	
	
	
	
	
	
	

}
