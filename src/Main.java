package consiste;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;



public class Main {
	
	public static void main(String[] args) {
		

		String path ="dataset.CSV";
		List<Pessoa> lista = new ArrayList<Pessoa>();
				
		
		try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path),"ISO-8859-1"))){
			
			String line = br.readLine();
			
			while((line = br.readLine()) != null) {
				
				String[] listaPessoa = line.split(";");
								
				if(listaPessoa.length < 4 && listaPessoa[0] != null && listaPessoa[1] != null) {
					
					String primeiroNome = listaPessoa[0].toUpperCase().trim().replace("\\s+", " ");
					String sobreNome = listaPessoa[1].toUpperCase().trim().replaceAll("\\s+", " ");
					double peso = 0;
					double altura = 0;
					double imc = 0;
										
					Pessoa pessoa = new Pessoa(primeiroNome,sobreNome, peso, altura, imc);
					lista.add(pessoa);
					continue;
										
				}else {
							
					String primeiroNome = listaPessoa[0].toUpperCase().trim().replace("\\s+", " ");
					String sobreNome = listaPessoa[1].toUpperCase().trim().replaceAll("\\s+", " ");
			     	double peso = Double.parseDouble(listaPessoa[2].replaceAll(",", "."));
			     	double altura = Double.parseDouble(listaPessoa[3].replaceAll(",", "."));
					double imc = peso / (Math.pow(altura, 2));
									
				Pessoa pessoa = new Pessoa(primeiroNome,sobreNome, peso, altura, imc);
					lista.add(pessoa);
				}
														
			}
			
		FileWriter arq = new FileWriter("Luis Henrique Biset Santos.txt");
		PrintWriter gravarArq = new PrintWriter(arq);			 
			 
		for(Pessoa p : lista) {	
			//FileWriter arquivoNome = new FileWriter(p.getPrimeiroNome() + p.getSobreNome());
			//PrintWriter gravarNome= new PrintWriter(arquivoNome);
			//gravarNome.printf("%s"+" "+"%s"+" "+"%.2f\n",p.getPrimeiroNome(),p.getSobreNome(),p.getImc());
			gravarArq.printf("%s"+" "+"%s"+" "+"%.2f\n",p.getPrimeiroNome(),p.getSobreNome(),p.getImc());
											
			//arquivoNome.close();
			}
		
		arq.close();
			
		}
		catch (Exception e) {
			System.out.println("Error" + e.getMessage());
		}
				
	}
		
}


